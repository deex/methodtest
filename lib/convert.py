#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Copyright (c) 2017 Damien Bataille.
# Author: Damien Bataille
# Email: dmnbataille (at) gmail (dot) com
# Created: 30/nov./2017 11:41

import itertools


def nestedArrayIntToFlatArray(arrayInt, sortR=True):
    """
    Flatten an array of arbitrarily nested arrays of integers into a flat array of integers. e.g. [[1,2,[3]],4] -> [1,2,3,4].
    :param arrayInt: array of arbitrarily nested arrays of integers
    :type arrayInt: list
    :param sortR: sort the result or not
    :type sortR: bool
    :return: a flat array of integers
    :rtype: list
    """
    finalList = []
    for item in arrayInt:
        if isinstance(item, (list, tuple)):
            finalList.extend(nestedArrayIntToFlatArray(item,
                                                       sortR=sortR
                                                       )
                             )
        else:
            finalList.append(item)
    return sorted(finalList) if sortR else finalList

'''
Test App
'''
if __name__ == "__main__":
    a = [1, 2, [3, [3, 4]], [[5, 6], [7, 8]], 2, 1]
    print nestedArrayIntToFlatArray(a, sortR=False)