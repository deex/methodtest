#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Copyright (c) 2017 Damien Bataille.
# Author: Damien Bataille
# Email: dmnbataille (at) gmail (dot) com
# Created: 30/nov./2017 14:24

import logging
import unittest

logging.basicConfig(level=logging.INFO)

class TempTracker(object):

    def __init__(self):
        self.logger = logging.getLogger(__name__)
        # current temperatures
        self.temperature = []
        self._get_max = None
        self._get_min = None
        self._get_mean = None

    def insert(self, toAdd):
        '''
        Records a new temperature. Must be an integer and between 0 and 1100
        :param toAdd: temperature to add to the current temperature
        :type toAdd: int
        :return: None
        :rtype: None
        '''
        if not isinstance(toAdd, int):
            message = 'Please insert an integer. Current type is {}'.format(type(toAdd).__name__)
            self.logger.error(message)
            raise Exception(message)
        if not toAdd >= 0 or not toAdd <= 1100:
            message = 'Please insert a temperature between 0 and 1100. Current value is {}'.format(toAdd)
            self.logger.error(message)
            raise Exception(message)
        self.temperature.append(toAdd)

    @property
    def get_max(self):
        '''
        Returns the highest temp we've seen so far
        :return: highest temperature
        :rtype: int
        '''
        return max(self.temperature)

    @property
    def get_min(self):
        '''
        Returns the lowest temp we've seen so far
        :return: lowest temperature
        :rtype: int
        '''
        return min(self.temperature)

    @property
    def get_mean(self):
        '''
        Returns the mean of all temps we've seen so far
        :return: mean of all temperatures
        :rtype: float
        '''
        return sum(self.temperature, 0.0) / len(self.temperature)


class MyTest(unittest.TestCase):

    def testInsert(self):
        toto = TempTracker()
        toto.insert(189)
        toto.insert(1002)
        toto.insert(10)
        toto.insert(102)
        self.assertEqual(toto.temperature,
                         [189, 1002, 10, 102]
                         )

    def testMax(self):
        toto = TempTracker()
        toto.insert(189)
        toto.insert(1002)
        toto.insert(10)
        toto.insert(102)
        self.assertEqual(toto.get_max,
                         1002
                         )

    def testMin(self):
        toto = TempTracker()
        toto.insert(189)
        toto.insert(1002)
        toto.insert(10)
        toto.insert(102)
        self.assertEqual(toto.get_min,
                         10
                         )

    def testMean(self):
        toto = TempTracker()
        toto.insert(189)
        toto.insert(1002)
        toto.insert(10)
        toto.insert(102)
        self.assertEqual(toto.get_mean,
                         325.75
                         )

if __name__ == "__main__":
    unittest.main()