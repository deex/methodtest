Welcome to MethodStudios!
===================

1) **Write some code, that will flatten an array of arbitrarily nested arrays of integers into a flat array of integers. e.g. [[1,2,[3]],4] -> [1,2,3,4].**

Use the file convert.py in lib, function nestedArrayIntToFlatArray()

2) **Write a class TempTracker** 

Use the file tempTracker.py

----------

> **Note:**

> - For the temperature tracker, I have replaced all "get" methods by attributes properties. It looks like more logical for me.
> - For the convert, I can use a nerdy list comprehension. But it was not very readable after.